package com.skywell.syngenta.domain.repository;

import com.skywell.syngenta.domain.entity.User;
import org.springframework.data.repository.CrudRepository;

/**
 * @author abohaichuk
 */
public interface UserRepository extends CrudRepository<User, Long> {
    User findByLogin(String login);
}
