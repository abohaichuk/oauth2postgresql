package com.skywell.syngenta.domain.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;

/**
 * @author abohaichuk
 */
@Getter
@Setter
@Embeddable
public class Address {

    private String city;

    private String street;

    private String apartment;

    private String zipCode;

}
