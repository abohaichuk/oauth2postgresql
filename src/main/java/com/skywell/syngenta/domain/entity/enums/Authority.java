package com.skywell.syngenta.domain.entity.enums;

import org.springframework.security.core.GrantedAuthority;

/**
 * @author abohaichuk
 */
public enum Authority implements GrantedAuthority {
    ROLE_USER, ROLE_ACCOUNT_MANAGER, ROLE_PROJECT_ACCEPT, ROLE_PROJECT_ADM, ROLE_SYSTEM_ADM;

    @Override
    public String getAuthority() {
        return toString();
    }
}
