package com.skywell.syngenta.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * @author abohaichuk
 */
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties("db.syngenta.datasource")
@EnableJpaRepositories(
        basePackages = "com.skywell.syngenta.domain",
        entityManagerFactoryRef = "syngentaEntityManagerFactory",
        transactionManagerRef = "syngentaTransactionManager"
)
public class DbConfig {
    private String driverClassName;
    private String url;
    private String username;
    private String password;

    @Value("${db.syngenta.hibernate.dialect}")
    private String dialect;
    @Value("${db.syngenta.hibernate.hbm2ddl.auto}")
    private String hbm2ddl;

    @Bean
    public HikariConfig hikariConfig() {
        HikariConfig dataSource = new HikariConfig();
        dataSource.setPoolName("springHikariCP");
        dataSource.setDriverClassName(driverClassName);
        dataSource.setJdbcUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        dataSource.setMaximumPoolSize(50);
        dataSource.setConnectionTimeout(15000);
        dataSource.setMaxLifetime(30000);
        return dataSource;
    }

    @Bean
    @Primary
    public DataSource syngentaDataSource() {
        return new HikariDataSource(hikariConfig());
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean syngentaEntityManagerFactory() {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(syngentaDataSource());
        entityManagerFactoryBean.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        Properties jpaProperties = new Properties();
        jpaProperties.setProperty(org.hibernate.cfg.Environment.DIALECT, dialect);
        jpaProperties.setProperty(org.hibernate.cfg.Environment.HBM2DDL_AUTO, hbm2ddl);
        jpaProperties.setProperty("hibernate.show_sql", "true");

        //---------    подключает новые генераторы, не используеться в jpa   --------
        //jpaProperties.setProperty("hibernate.id.new_generator_mappings", "true");
        entityManagerFactoryBean.setJpaProperties(jpaProperties);
        entityManagerFactoryBean.setPackagesToScan(new String[] {"com.skywell.syngenta.domain"});
        return entityManagerFactoryBean;
    }

    @Bean
    public JpaTransactionManager syngentaTransactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(syngentaEntityManagerFactory().getObject());
        return transactionManager;
    }

    public String getDriverClassName() {
        return driverClassName;
    }

    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDialect() {
        return dialect;
    }

    public void setDialect(String dialect) {
        this.dialect = dialect;
    }

    public String getHbm2ddl() {
        return hbm2ddl;
    }

    public void setHbm2ddl(String hbm2ddl) {
        this.hbm2ddl = hbm2ddl;
    }
}
