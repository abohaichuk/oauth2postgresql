package com.skywell.syngenta.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @author abohaichuk
 */
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private SyngentaDetailsService syngentaDetailsService;

    public WebSecurityConfig(@Lazy SyngentaDetailsService syngentaDetailsService) {
        this.syngentaDetailsService = syngentaDetailsService;
    }

    @Autowired
    public void globalUserDetails(final AuthenticationManagerBuilder auth) throws Exception {
        auth
                .userDetailsService(getUserDetails());
    }

    @Bean
    public UserDetailsService getUserDetails() {
        return syngentaDetailsService;
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }



    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http
                .cors().and().anonymous().and().csrf().disable()
                .authorizeRequests()
                .antMatchers("/login").permitAll()
                //.antMatchers(HttpMethod.POST, "/login").permitAll()
                .antMatchers(HttpMethod.POST, "/food").permitAll()
                .anyRequest().authenticated().and()
                .formLogin().permitAll();
    }
}
