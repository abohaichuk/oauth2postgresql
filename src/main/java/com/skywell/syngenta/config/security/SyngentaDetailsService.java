package com.skywell.syngenta.config.security;

import com.skywell.syngenta.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @author abohaichuk
 */
@Service
public class SyngentaDetailsService implements UserDetailsService {

    @Autowired
    UserService userService;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        UserDetails details = userService.findByLogin(login);
        if (Objects.isNull(details))
            throw new UsernameNotFoundException(login);
        return details;
    }
}
