package com.skywell.syngenta.service;

import com.skywell.syngenta.domain.entity.User;

/**
 * @author abohaichuk
 */
public interface UserService {
    User findByLogin(String login);
    User save(User user);
}
